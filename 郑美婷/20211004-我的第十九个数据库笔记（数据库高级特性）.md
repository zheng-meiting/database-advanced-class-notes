# 数据库高级应用设计（基础）

## 数据库有哪些“高级特性”

1. 存储过程 
2. 视图 
3. 触发器
4. 游标  
5. 事务 
6. 自定义方法


![图裂了！](./img/20211004上课目标.jpg)

# 老胡笔记

## 存储过程定义（数据库）

```

-- 创建存储过程语法

create proc proc_SelectStudentInfo
as
begin
	select * from StudentInfo
end


-- 执行存储过程

exec proc_SelectStudentInfo
go

-- 带参数的存储过程

create proc proc_SelectStudentInfoWithParameter
@name nvarchar(80)
as
begin
	select * from StudentInfo
	where StudentName like @name
end
go

exec proc_SelectStudentInfoWithParameter '%雷%'
go

-- 带多个参数的存储过程

create proc proc_SelectStudentInfoWithSomeParameter
@name nvarchar(80),
@code nvarchar(80)
as

begin
	select * from StudentInfo
	where StudentCode like @code and StudentName like @name
end
go

exec proc_SelectStudentInfoWithSomeParameter '%雷%','01'
go

```
> go ：批处理，到了此条语句立马执行（防止报错）  
> **存储过程**其功能就是方便调用

---
---

# 创建存储过程SQL语法步骤

![图裂了！](./img/创建存储过程语法1.jpg)
![图裂了！](./img/创建存储过程语法2.jpg)
![图裂了！](./img/创建存储过程语法3.jpg)
![图裂了！](./img/创建存储过程语法4.jpg)
![图裂了！](./img/创建存储过程语法5.jpg)
![图裂了！](./img/创建存储过程语法6.jpg)
![图裂了！](./img/创建存储过程语法7.jpg)
![图裂了！](./img/创建存储过程语法8.jpg)
![图裂了！](./img/创建存储过程语法9.jpg)
![图裂了！](./img/创建存储过程语法10.jpg)
![图裂了！](./img/创建存储过程语法11.jpg)
![图裂了！](./img/创建存储过程语法12.jpg)
![图裂了！](./img/创建存储过程语法13.jpg)

## 比记事本好用的软件get！

![图裂了吧!](./img/20211004打开.jpg)

# **码农笔记**

### 1.小知识点get！**游标**是为了遍历数据集里面的每一条记录，经过这两三年的时间，现在已经不常用了。

### 2.过程其实就是函数和方法

### 3.自定方法可返回查询集

### 4.做事情不要应付，要持之以恒地认真去做！

### 5.技术和说话（沟通能力）同等重要！

### 6.想好自己以后究竟要走一条什么样的路......

### 7.有一种职业叫做架构师，对未来的职业规划还是了解太少了！（启动度娘！）系统架构师是一个最终确认和评估系统需求，给出开发规范，搭建系统实现的核心构架，并澄清技术细节、扫清主要难点的技术人员。主要着眼于系统的“技术实现”。 系统架构师负责设计系统整体架构，从需求到设计的每个细节都要考虑到，把握整个项目，使设计的项目尽量效率高，开发容易，维护方便，升级简单等。系统构架，是对已确定的需求的技术实现构架、作好规划，运用成套、完整的工具，在规划的步骤下去完成任务。

### 8.**ping SQL语句** 是一种入侵方式  

### 9.视图就是*PowerDesigner*里的表格  

### 10.双十一要到了，要掏钱买服务器了，我的奶茶没了T▽T（买完服务器，重新注册用户，可以学Winners，不知道是不是这么拼，嘿嘿(●ˇ∀ˇ●)，回头可以自己建一个简单的网站了，心动了！）  
![图裂了！](./img/大爷快乐.jpg)
