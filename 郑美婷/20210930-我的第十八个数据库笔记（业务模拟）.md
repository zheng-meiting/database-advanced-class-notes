# 业务模拟

## 老胡笔记  

``` 
-- 20210930 SQL注入

declare @username nvarchar(80),@password nvarchar(80)

select * from Users
where Username=@username and Password=@password


set @password='1 or 1=1'


select * from Users
where Username=@username and Password=1 or 1=1


-- 网站信息业务 网站名称、logo图片、简介和工信部备案、公安备案业务是分开设置的

-- 1. 系统刚刚上线，里面并没有网站的信息 所以它是插入操作

insert into WebSiteInfo (WebSiteName,LogoFileId,ShortDesc) values ('','','')

-- 更新1条记录的时候，请记得总是使用id作为查询条件
update WebSiteInfo set ICPNo='', PublicSafeNo=''
where Id=1

-- 2. 之前已经设置过网站信息，那么接下来的设置反应在sql操作上，都更新操作


update WebSiteInfo set WebSiteName='',LogoFileId='',ShortDesc=''
where id=1

update WebSiteInfo set ICPNo='', PublicSafeNo=''
where Id=1

-- 文章分类信息 业务主要就使用场景就是在设置（添加、修改）文章的时候
 -- 需要在下拉列表框当中，选择一个栏目


 select Id,TypeName from ArticleTypeInfo
 where IsDeleted=0 and IsActived=1
 order by DisplayOrder

 -- 1.添加文章分类信息
	insert into ArticleTypeInfo (TypeName) values ('校园活动')

 -- 2.更新文章分类信息（两个方面理解，一是更新是指数据库层面的更新，另一种更新就是单纯的指更新分类名称，这里采用后一种理解）
	update ArticleTypeInfo set TypeName=''
	where Id=3

 -- 3.删除指定分类
	update ArticleTypeInfo set IsDeleted=1
	where Id=4
 -- 4.禁用指定分类
	update ArticleTypeInfo set IsActived=0
	where Id=6
 --	5.更新分类的显示顺序
	update ArticleTypeInfo set DisplayOrder=99
	where Id=7

```

---
---

## 如何避免SQL语句注入

### 什么是SQL注入攻击？

 sql注入是一种将sql代码添加到输入参数中，传递到sql服务器解析并执行的一种攻击手法。

### SQL注入是如何产生的？

1. web开发人员无法保证所有的输入都已经过滤。

2. 攻击者利用发送给sql服务器的输入数据构造可执行代码。

3. 数据库未作相应的安全配置（对web应用设置特定的数据库账号，而不使用root或管理员账号，特定数据库账号给予一些简单操作的权限，回收一些类似drop的操作权限）。

### SQL注入攻击防御

1. 使用参数化筛选语句

 为了防止SQL注入，用户输入不能直接嵌入到SQL语句中。相反，用户输入必须被过滤或参数化。参数语句使用参数，而不是将用户输入嵌入语句中。在大多数情况下，SQL语句是正确的。然后，用户输入仅限于一个参数。
  
 一般来说，有两种方法可以确保应用程序不易受到SQL注入攻击。一种是使用代码审查，另一种是强制使用参数化语句。强制使用参数化语句意味着在运行时将拒绝嵌入用户输入中的SQL语句。但是，目前对此功能的支持不多。
  
2. 避免使用解释程序，这是黑客用来执行非法命令的手段。

3. 防止SQL注入，但也避免一些详细的错误消息，因为黑客可以使用这些消息。标准的输入验证机制用于验证所有输入数据的长度、类型、语句和企业规则。

4. 使用专业的漏洞扫描工具。

 但是，防范SQL注入攻击是不够的。攻击者现在自动搜索和攻击目标。它的技术甚至可以很容易地应用于其他Web体系结构中的漏洞。企业应该投资于专业的漏洞扫描工具，如著名的Accunetix网络漏洞扫描程序。完美的漏洞扫描器不同于网络扫描器，它专门在网站上查找SQL注入漏洞。最新的漏洞扫描程序可以找到最新发现的漏洞。

5. 最后，企业在Web应用程序开发过程的所有阶段执行代码安全检查。首先，安全测试应该在部署Web应用程序之前实现，这比以前更重要、更深远。企业还应在部署后使用漏洞扫描工具和站点监控工具测试网站。

# **码农笔记**  

### 1.当代划水大师郑大爷  
![图裂了！](./img/大爷.jpeg)  

### 2.永远不是在补笔记，就是在修改笔记的路上，成功治愈懒癌患者TvT
